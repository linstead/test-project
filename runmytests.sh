#!/bin/bash

# check that output matches what we expect

output=$(./mybinary)
if [ "$output" == "Hello, World!" ]; then
    exit 0;
else
    exit 1;
fi

# EOF
